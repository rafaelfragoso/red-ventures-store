# Red Ventures Nike Store

Esse é o resultado do teste que foi me passado por e-mail pela Red Ventures.

## Instalando e Rodando

```npm install``` e ```npm run start```

e ```npm run bundle``` para build em produção.

## Stack Escolhida

- React
- Redux
- Webpack
- SASS/PostCSS
- ES6

## Possíveis Melhorias

- CSS Atomic Design (patternlab.io)
- Ajustar para dispositivos móveis
- Refatorar o component de filtros
- Implementar carrinho de compras
- Testar a aplicação com o Enzyme