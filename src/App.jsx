import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers';
import './App.scss';
import RedProductsContainer from './containers/RedProductsContainer';
import RedHeader from './components/RedHeader/RedHeader';
import RedFooter from './components/RedFooter/RedFooter';
import products from '../public/data.json';

const initialState = Object.assign({}, products, {
  visibilityFilter: {}, 
  cart: [] // implement cart feature
});

const store = createStore(reducer, initialState);

class NikeApp extends React.Component {
  render() {
    return (
      <div>
        <RedHeader showBanner />

        <RedProductsContainer />

        <RedFooter />
      </div>
    )
  }
}

render( 
  <Provider store={store}>
    <NikeApp />
  </Provider>, 
  document.getElementById( 'nike-app' ) 
);