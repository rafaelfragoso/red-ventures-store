export const applyFilters = (filters) => {
  return {
    type: 'APPLY_FILTERS', 
    payload: {
      filters
    }
  };
};

export const removeFilters = () => {
  return {
    type: 'REMOVE_FILTERS', 
    payload: {}
  };
};