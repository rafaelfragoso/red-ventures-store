import React from 'react'

export default class RedFooter extends React.Component {
  render() {
    return (
      <footer>
        <div className="banner-footer">
          <h2 title="Just Do It.">Just Do It.</h2>
          <a href="/" className="all-products-btn" title="Todos os Produtos">Todos os produtos</a>
        </div>
        <div className="copyright">
          <p>Nike Copyright 2017 - all rights reserved</p>
        </div>
      </footer>
    );
  }
}