import React from 'react';
import RedNavigation from '../RedNavigation/RedNavigation';

export default class RedHeader extends React.Component {
  constructor(props) {
    super(props);  
  }

  render() {
    return (
      <header>
        <RedNavigation />
        <div 
          className="banner" 
          style={{
            display: this.props.showBanner ? 'block' : 'none',
            visibility: this.props.showBanner ? 'visible' : 'hidden',
          }}
        ></div>
      </header> 
    );
  }
}