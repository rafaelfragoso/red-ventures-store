import React from 'react'

export default class RedNavigation extends React.Component {
  render() {
    return (
      <nav>
        <div className="container">
          <h1 id="logo" title="Nike">Nike</h1> 
          <ul className="nav-top">
            <li><a href="#" title="Masculino">Masculino</a></li>
            <li><a href="#" title="Feminino">Feminino</a></li>
            <li><a href="#" title="Menino">Menino</a></li>
            <li><a href="#" title="Menina">Menina</a></li>
          </ul>

          <ul className="nav-top-cart">
            <li><a href="#" title="Carrinho de Compras">Carrinho de Compras</a></li>
          </ul>
        </div>
      </nav>  
    );
  }
}