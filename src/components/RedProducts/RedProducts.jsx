import React from 'react'
import ReactDOM from 'react-dom'
import './RedProducts.scss'
import RedProductsFilter from './RedProductsFilter';
import RedProductsList from './RedProductsList';

export default class RedProducts extends React.Component {
  render() {
    return (
      <section id="products">
        <div className="container">
          
          <RedProductsFilter 
            filterTitle="Chuteiras HyperVenom" 
            applyFilters={this.props.applyFilters}
            removeFilters={this.props.removeFilters}
          />

          <RedProductsList productTitle="Mais Vendidos" products={this.props.bestSellers} />
          <RedProductsList productTitle="Lançamentos" products={this.props.releases} />
          
        </div>
      </section>
    );
  }
}