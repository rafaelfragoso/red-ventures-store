import React from 'react'
import ReactDOM from 'react-dom'
import './RedProductsFilter.scss'
import {map} from 'lodash';

export default class RedProductsFilter extends React.Component {
  generateFilters(filters) {
    return Object.assign({}, this.getHighTopFilters(filters), this.getCategoryFilters(filters));
  }

  getHighTopFilters(filters) {
    if (!filters.highTopTrue.checked && !filters.highTopFalse.checked) {
      return null;
    } else if (filters.highTopTrue.checked && !filters.highTopFalse.checked) {
      return {'high-top': true};
    } else if (!filters.highTopTrue.checked && filters.highTopFalse.checked) {
      return {'high-top': false};
    } else {
      return null;
    }
  }

  getCategoryFilters(filters) {
    if (!filters.categorySociety.checked && !filters.categoryCampo.checked) {
      return null;
    } else if (filters.categorySociety.checked && !filters.categoryCampo.checked) {
      return {'category': 'society'};
    } else if (!filters.categorySociety.checked && filters.categoryCampo.checked) {
      return {'category': 'campo'};
    } else {
      return null;
    }
  }

  applyFilters() {
    let filters = this.generateFilters(this.refs);

    this.props.applyFilters(filters);
  }

  removeFilters() {
    let filters = this.refs;

    map(filters, (f) => {
      return f.checked = false;
    });

    this.props.removeFilters();
  }

  render() {
    return (
      <div>
        <div className="filters">
          <ul>
            <li className="filter-title">{this.props.filterTitle}:</li>
            <li>
              <input type="checkbox" 
                     id="highTopTrue"
                     className="filter-options" 
                     ref="highTopTrue"
                     onChange={this.applyFilters.bind(this)} />
              <label htmlFor="highTopTrue"><span></span>Cano Alto</label>
            </li>
            <li>
              <input type="checkbox" 
                     id="highTopFalse" 
                     className="filter-options" 
                     ref="highTopFalse"
                     onChange={this.applyFilters.bind(this)} />
              <label htmlFor="highTopFalse"><span></span>Cano Baixo</label>
            </li>
            <li>
              <input type="checkbox" 
                     id="categoryCampo" 
                     className="filter-options" 
                     ref="categoryCampo"
                     onChange={this.applyFilters.bind(this)} />
              <label htmlFor="categoryCampo"><span></span>Futebol Campo</label>
            </li>
            <li>
              <input type="checkbox" 
                     id="categorySociety" 
                     className="filter-options" 
                     ref="categorySociety"
                     onChange={this.applyFilters.bind(this)} />
              <label htmlFor="categorySociety"><span></span>Futebol Society</label>
            </li>
          </ul>
          <button 
            className="all-products-btn" 
            title="Todos os Produtos"
            onClick={this.removeFilters.bind(this)}>
              Todos os produtos
          </button>
        </div>
      </div> 
    );
  }
}