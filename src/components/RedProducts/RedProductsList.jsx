import React from 'react';
import './RedProductsList.scss';
import RedProductsListItem from './RedProductsListItem';

export default class RedProductsList extends React.Component {
  constructor(props) {
    super(props);
  }

  renderItems() {
    return this.props.products.map( (data, index) => <RedProductsListItem key={index} {...data} />)
  }

  render() {
    return (
      <div>
        <div className="products-slider">
          <h2 className="products-title" title={this.props.productTitle}>{this.props.productTitle}</h2>
          <ul className="products-list">
            {this.props.products.map( (data, index) =>
              <RedProductsListItem key={index} {...data} />
            )}
          </ul>  
        </div>
      </div>
    );
  }
}