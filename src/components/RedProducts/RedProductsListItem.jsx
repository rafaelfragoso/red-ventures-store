import React from 'react'

export default class RedProductsListItem extends React.Component {
  render() {
    return (
      <li>
        <div className="products-list_header">
          <img src={this.props.image} />
          <div className="products-list_header-personalize">
            <img src="images/personalize.jpg" />
            <span>Personalize</span>
          </div>
        </div>
        <div className="products-list_body">
          <p className="products-list_body-title">{this.props.title}</p>
          <p className="products-list_body-category">{this.props.high-top ? 'Cano Alto' : 'Cano Baixo'}</p>
          <p className="products-list_body-price">R${this.props.price.toFixed(2)}</p>
          <p className="products-list_body-installments">ou {this.props.installments.number}X {this.props.installments.value.toFixed(2)} sem juros</p>
        </div>
        <div className="products-list_actions">
          <a href="#" className="buy-btn">Comprar</a>
        </div>
      </li>
    );
  }
}