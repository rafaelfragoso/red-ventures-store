import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {filter, isEmpty} from 'lodash';
import RedProducts from '../components/RedProducts/RedProducts';
import {applyFilters, removeFilters} from '../actions';

const getVisibleProducts = (products, filters) => {
  if ( isEmpty(filters) ) {
    return products;
  }

  return filter(products, filters);
};

const mapStateToProps = (state) => ({
  bestSellers: getVisibleProducts(state.bestSellers, state.visibilityFilter),
  releases: getVisibleProducts(state.releases, state.visibilityFilter)
});

const matchDispatchToProps = (dispatch) => {
  return bindActionCreators({
    applyFilters,
    removeFilters
  }, dispatch);
};

export default connect(mapStateToProps, matchDispatchToProps)(RedProducts);