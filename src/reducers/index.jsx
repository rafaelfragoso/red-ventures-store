import { combineReducers } from 'redux';
import cart from './cart';
import bestSellers from './bestSellers';
import releases from './releases';
import visibilityFilter from './visibilityFilter';

const nikeApp = combineReducers({
  cart,
  visibilityFilter,
  bestSellers,
  releases
});

export default nikeApp;